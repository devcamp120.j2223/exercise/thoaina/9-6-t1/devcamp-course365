// import course model
const { mongoose } = require("mongoose");
const courseModel = require("../models/courseModel");

// get all courses
const getAllCourse = (req, res) => {
    courseModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getCourseById = (req, res) => {
    let id = req.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        courseModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const createCourse = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.courseCode) {
        res.status(400).json({
            message: "courseCode is require!",
        })
    }
    else if (!body.courseName) {
        res.status(400).json({
            message: "courseName is require!",
        })
    }
    else if (!Number.isInteger(body.price) || body.price < 0) {
        res.status(400).json({
            message: "price is invalid!",
        })
    }
    else if (!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
        res.status(400).json({
            message: "discountPrice is invalid!",
        })
    }
    else if (!body.duration) {
        res.status(400).json({
            message: "duration is require!",
        })
    }
    else if (!body.level) {
        res.status(400).json({
            message: "level is require!",
        })
    }
    else if (!body.coverImage) {
        res.status(400).json({
            message: "coverImage is require!",
        })
    }
    else if (!body.teacherName) {
        res.status(400).json({
            message: "teacherName is require!",
        })
    }
    else if (!body.teacherPhoto) {
        res.status(400).json({
            message: "teacherPhoto is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let course = {
            _id: mongoose.Types.ObjectId(),
            courseCode: body.courseCode,
            courseName: body.courseName,
            price: body.price,
            discountPrice: body.discountPrice,
            duration: body.duration,
            level: body.level,
            coverImage: body.coverImage,
            teacherPhoto: body.teacherPhoto,
            teacherName: body.teacherName
        };
        courseModel.create(course, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateCourseById = (req, res) => {
    let id = req.params.courseId;
    let body = req.body;

    if (!body.courseCode) {
        res.status(400).json({
            message: "courseCode is require!",
        })
    }
    else if (!body.courseName) {
        res.status(400).json({
            message: "courseName is require!",
        })
    }
    else if (!Number.isInteger(body.price) || body.price < 0) {
        res.status(400).json({
            message: "price is invalid!",
        })
    }
    else if (!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
        res.status(400).json({
            message: "discountPrice is invalid!",
        })
    }
    else if (!body.duration) {
        res.status(400).json({
            message: "duration is require!",
        })
    }
    else if (!body.level) {
        res.status(400).json({
            message: "level is require!",
        })
    }
    else if (!body.coverImage) {
        res.status(400).json({
            message: "coverImage is require!",
        })
    }
    else if (!body.teacherName) {
        res.status(400).json({
            message: "teacherName is require!",
        })
    }
    else if (!body.teacherPhoto) {
        res.status(400).json({
            message: "teacherPhoto is require!",
        })
    }
    else {
        let course = {
            courseCode: body.courseCode,
            courseName: body.courseName,
            price: body.price,
            discountPrice: body.discountPrice,
            duration: body.duration,
            level: body.level,
            coverImage: body.coverImage,
            teacherPhoto: body.teacherPhoto,
            teacherName: body.teacherName
        };
        courseModel.findByIdAndUpdate(id, course, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteCourseById = (req, res) => {
    let id = req.params.courseId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        courseModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllCourse, getCourseById, createCourse, updateCourseById, deleteCourseById };
