// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    courseCode: {
        type: String,
        require: true,
        unique: true,
    },
    courseName: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    discountPrice: {
        type: Number,
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    level: {
        type: String,
        required: true,
    },
    coverImage: {
        type: String,
        required: true,
    },
    teacherName: {
        type: String,
        required: true,
    },
    teacherPhoto: {
        type: String,
        required: true,
    },
    teacherName: {
        type: Boolean,
        default: true,
    },
    teacherPhoto: {
        type: Boolean,
        default: false,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Course", courseSchema);