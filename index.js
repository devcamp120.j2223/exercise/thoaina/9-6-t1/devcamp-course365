// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/CRUD_Course365", function (error) {
    if (error) throw error;
    console.log('Successfully connected');
})

const courseModel = require("./app/models/courseModel");
const courseRouter = require("./app/routers/courseRouter");

// Khai báo thư viện path
const path = require("path");

// Khởi tạo app express
const app = express();
app.use(express.json());
app.use(express.urlencoded({
    urlencoded: true,
}));
// Khai báo cổng của project
const port = 8000;

// Khai báo sử dụng tài nguyên
app.use(express.static("views"));

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"));
})

app.get("/crudCourses.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/crudCourses.html"));
})
app.use("/", courseRouter);
// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})